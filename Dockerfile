FROM "alpine:3.9"
ENV WORKDIR="/workdir"
WORKDIR "${WORKDIR}"
RUN \
	set -e ;\
	set -u ;\
	apk update ;\
	apk add -u \
		binutils=2.31.1-r2 \
		m4=1.4.18-r1 \
		bison=3.0.5-r0 \
		ca-certificates=20190108-r0 \
		libattr=2.4.47-r7 \
		libacl=2.2.52-r5 \
		libbz2=1.0.6-r6 \
		expat=2.2.6-r0 \
		lz4-libs=1.8.3-r2 \
		xz-libs=5.2.4-r0 \
		libarchive=3.3.2-r4 \
		nghttp2-libs=1.35.1-r0 \
		libssh2=1.8.2-r0 \
		libcurl=7.64.0-r2 \
		ncurses-terminfo-base=6.1_p20190105-r0 \
		ncurses-terminfo=6.1_p20190105-r0 \
		ncurses-libs=6.1_p20190105-r0 \
		libgcc=8.3.0-r0 \
		rhash-libs=1.3.6-r2 \
		libstdc++=8.3.0-r0 \
		libuv=1.23.2-r0 \
		cmake=3.13.0-r0 \
		doxygen=1.8.15-r0 \
		libmagic=5.36-r0 \
		file=5.36-r0 \
		flex=2.6.4-r1 \
		gmp=6.1.2-r1 \
		isl=0.18-r0 \
		libgomp=8.3.0-r0 \
		libatomic=8.3.0-r0 \
		mpfr3=3.1.5-r1 \
		mpc1=1.0.3-r1 \
		gcc=8.3.0-r0 \
		musl-dev=1.1.20-r4 \
		libc-dev=0.7.1-r0 \
		g++=8.3.0-r0 \
		libffi=3.2.1-r6 \
		gdbm=1.13-r1 \
		readline=7.0.003-r1 \
		sqlite-libs=3.28.0-r0 \
		python3=3.6.8-r2 \
		gdb=8.2-r1 \
		pcre2=10.32-r1 \
		git=2.20.1-r0 \
		go=1.11.5-r0 \
		linux-headers=4.18.13-r1 \
		lua5.3-libs=5.3.5-r2 \
		make=4.2.1-r2 \
		screen=4.6.2-r0 \
		sudo=1.8.25_p1-r2 \
		tree=1.8.0-r0 \
		valgrind=3.14.0-r0 \
		vim=8.1.1365-r0 ;\
	mkdir -p "${WORKDIR}" ;\
	chmod 1777 "${WORKDIR}" ;
